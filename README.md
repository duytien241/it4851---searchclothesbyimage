# IT4851 - SearchClothesByImage

Cài đặt pipenv

```bash
pip install -U pip pipenv
```
Cài đặt thư viện

```bash
pipenv install --system --deploy
```

Thiết lập server

```bash
cd server
python manage.py makemigrations
python manage.py migrate
```

Chạy server

```bash
cd server
python manage.py runserver
```

Thiết lập frontend
```bash
cd server/frontend
npm install
```

Chạy frontend
```bash
cd server/frontend
npm start
```
