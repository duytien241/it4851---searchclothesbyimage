import React, { Component } from 'react';
import { Upload, Icon, Switch, Button, notification, Select, Form } from 'antd';
import './css/style.css';

const { Dragger } = Upload;
const { Option } = Select;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      imageUrl: '',
      isOpenFiler: false
    }
  }

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleUploadChange = info => {
    const { status, originFileObj } = info.file;
    if (status === 'uploading') {
      this.setState({
        loading: true
      });
    } else if (status === 'done') {
      notification.success({
        message: 'Upload File',
        description: 'upload file success !!',
        duration: 2
      });
      this.getBase64(originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    } else if (status === 'error') {
      notification.error({
        message: 'Upload File',
        description: 'upload file error !!',
        duration: 2
      });
    }
  }
  handleSearchClothes = () => {
    const { isOpenFiler, imageUrl } = this.state;
    if (!imageUrl) {
      notification.error({
        message: 'Search Failure',
        description: 'You must be upload your image !!',
        duration: 2
      });
    } else {
      if (isOpenFiler) {
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            console.log({
              imageUrl,
              filterData: values
            });
          }
        })
      } else {
        console.log(imageUrl);
      }
    }
  }
  render() {
    const { imageUrl, loading, isOpenFiler } = this.state;
    const props = {
      accept: 'image/x-png,image/gif,image/jpeg',
      name: 'avatar',
      listType:"picture-card",
      multiple: true,
      className: 'avatar-uploader',
      onChange: this.handleUploadChange,
      showUploadList: false,
      action: "https://www.mocky.io/v2/5cc8019d300000980a055e76", // méo hiểu vì sao cần mock 1 api này mới upload được
    };
    const uploadButton = (<div>
      <p className="ant-upload-drag-icon">
        <Icon type={loading ? 'loading' : 'inbox'} />
      </p>
      <p className="ant-upload-text">Chọn hoặc thả file vào đây</p>
      <p className="ant-upload-hint">
        Tải một hoặc nhiều ảnh lên tại đây
      </p>
    </div>);
    const { form } = this.props;
    const { getFieldDecorator } = form;
    return (
      <React.Fragment>
        <div className="container">
          <div className="header">
            <h1>Find Your Clothes</h1>
            <h3>Dễ dàng, Nhanh chóng và Thuận tiện</h3>
          </div>
          <div className="body">
            <Dragger {...props}>
              {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '400px' }} /> : uploadButton}
            </Dragger>
            <div className="option" style={ isOpenFiler ? { backgroundColor: "rgba(0, 0, 0, 0.6)"} : null}>
              <div className="switch">
                <Switch defaultChecked={isOpenFiler} onChange={checked => this.setState({ isOpenFiler: checked })}/>
                <span>Giúp chúng tôi tìm nhanh hơn</span>
              </div>
              {isOpenFiler && <div className="selector">
                <Form layout="inline">
                  <Form.Item>
                  {getFieldDecorator('type', {})
                  (<Select placeholder="Loại áo">
                      <Option value="Sơ mi">Sơ mi</Option>
                      <Option value="T-shirt">T-shirt</Option>
                      <Option value="Áo khoác">Áo khoác</Option>
                  </Select>)
                  }
                  </Form.Item>
                  <Form.Item>
                  {getFieldDecorator('brand', {})
                  (<Select placeholder="Thương hiệu">
                    <Option value="Chanel">Chanel</Option>
                    <Option value="Dior">Dior</Option>
                    <Option value="Gucci">Gucci</Option>
                  </Select>)
                  }
                  </Form.Item>
                  <Form.Item>
                  {getFieldDecorator('gender', {})
                  (<Select placeholder="Giới tính">
                      <Option value="female">Female</Option>
                      <Option value="male">Male</Option>
                      <Option value="LGBT">LGBT</Option>
                    </Select>)
                  }
                  </Form.Item>
                </Form>
                </div>}
            </div>
            <Button type="primary" onClick={this.handleSearchClothes}>Tìm Kiếm Sản Phẩm</Button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Form.create({ name: 'Filter Data' })(App);
