from django.shortcuts import render
import matplotlib.pyplot as plt
from django.http import HttpResponse
from django.views import View
import cv2
import numpy as np
import os
import pickle
import imutils

# from .utils import compute_HOG
# Create your views here.


n_bin = 128
n_orient = 8
p_p_c = (32, 32)
c_p_b = (1, 1)


class ColorDescriptor:
    def __init__(self, bins):
		# store the number of bins for the 3D histogram
        self.bins = bins

    def describe(self):
		# convert the image to the HSV color space and initialize
		# the features used to quantify the image
        image = cv2.imread( './app/img/dac_nhan_tam.jpg', 1)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        features = []

		# grab the dimensions and compute the center of the image
        (h, w) = image.shape[:2]
        (cX, cY) = (int(w * 0.5), int(h * 0.5))

		# divide the image into four rectangles/segments (top-left,
		# top-right, bottom-right, bottom-left)
        segments = [(0, cX, 0, cY), (cX, w, 0, cY), (cX, w, cY, h),
			(0, cX, cY, h)]

		# construct an elliptical mask representing the center of the
		# image
        (axesX, axesY) = (int(w * 0.75) // 2, int(h * 0.75) // 2)
        ellipMask = np.zeros(image.shape[:2], dtype = "uint8")
        cv2.ellipse(ellipMask, (cX, cY), (axesX, axesY), 0, 0, 360, 255, -1)

		# loop over the segments
        for (startX, endX, startY, endY) in segments:
			# construct a mask for each corner of the image, subtracting
			# the elliptical center from it
            cornerMask = np.zeros(image.shape[:2], dtype = "uint8")
            cv2.rectangle(cornerMask, (startX, startY), (endX, endY), 255, -1)
            cornerMask = cv2.subtract(cornerMask, ellipMask)

			# extract a color histogram from the image, then update the
			# feature vector
            hist = self.histogram(image, cornerMask)
            features.extend(hist)

		# extract a color histogram from the elliptical region and
		# update the feature vector
        hist = self.histogram(image, ellipMask)
        features.extend(hist)

		# return the feature vector
        return features

    def histogram(self, image, mask):
		# extract a 3D color histogram from the masked region of the
		# image, using the supplied number of bins per channel
        hist = cv2.calcHist([image], [0, 1, 2], mask, self.bins,[0, 180, 0, 256, 0, 256])

		# normalize the histogram if we are using OpenCV 2.4
        if imutils.is_cv2():
            hist = cv2.normalize(hist).flatten()

		# otherwise handle for OpenCV 3+
        else:
            hist = cv2.normalize(hist, hist).flatten()

		# return the histogram
        return hist

def get_sift(input_data):
    img = cv2.imread( './app/img/dac_nhan_tam.jpg', 1)
    sift = cv2.xfeatures2d.SIFT_create()
    keypoints, descriptors = sift.detectAndCompute(img, None)
    img = cv2.drawKeypoints(img, keypoints, None)
    cv2.imshow("Image", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    index = []
    for point in keypoints:
        temp = (point.pt, point.size, point.angle, point.response, point.octave, point.class_id)
        print(temp)
        index.append(temp)
    # Dump the keypoints
    f = open("keypoints.txt", "wb")
    f.write(pickle.dumps(index))
    f.close()

    # im = cv2.imread("./app/img/dac_nhan_tam.jpg")
    # index = pickle.loads(open("keypoints.txt", 'rb').read())
    # kp = []

    # for point in index:
    #     temp = cv2.KeyPoint(x=point[0][0],y=point[0][1],_size=point[1], _angle=point[2], _response=point[3], _octave=point[4], _class_id=point[5]) 
    #     print(point)
    #     kp.append(temp)

    # Draw the keypoints
    imm = cv2.drawKeypoints(im, kp, None);

def get_histogram(input_data= './app/img/dac_nhan_tam.jpg', normalize=True):
    t = ColorDescriptor((8, 12, 3))
    print(t.describe())

class Searcher:
	def __init__(self, indexPath):
		# store our index path
		self.indexPath = indexPath

	def search(self, queryFeatures, limit = 10):
		# initialize our dictionary of results
		results = {}

		# open the index file for reading
		with open(self.indexPath) as f:
			# initialize the CSV reader
			reader = csv.reader(f)

			# loop over the rows in the index
			for row in reader:
				# parse out the image ID and features, then compute the
				# chi-squared distance between the features in our index
				# and our query features
				features = [float(x) for x in row[1:]]
				d = self.chi2_distance(features, queryFeatures)

				# now that we have the distance between the two feature
				# vectors, we can udpate the results dictionary -- the
				# key is the current image ID in the index and the
				# value is the distance we just computed, representing
				# how 'similar' the image in the index is to our query
				results[row[0]] = d

			# close the reader
			f.close()

		# sort our results, so that the smaller distances (i.e. the
		# more relevant images are at the front of the list)
		results = sorted([(v, k) for (k, v) in results.items()])

		# return our (limited) results
		return results[:limit]

	def chi2_distance(self, histA, histB, eps = 1e-10):
		# compute the chi-squared distance
		d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps)
			for (a, b) in zip(histA, histB)])

		# return the chi-squared distance
		return d
