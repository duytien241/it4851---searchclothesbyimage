from django.shortcuts import render
from skimage.feature import hog
from skimage import io
import matplotlib.pyplot as plt

import cv2
import numpy as np
import os
from django.http import HttpResponse

def compute_HOG( img, n_bin, normalize=True):
    fd = hog(img, orientations=n_orient, pixels_per_cell=p_p_c, cells_per_block=c_p_b,
             feature_vector=True, block_norm='L2-Hys', visualize=False, multichannel=True)
    hist, _ = np.histogram(fd, bins=n_bin)

    if normalize:
        hist = np.array(hist) / np.sum(hist)

    return hist
