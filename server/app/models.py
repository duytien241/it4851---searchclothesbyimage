from django.db import models
import uuid

class product(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  name = models.CharField(max_length=200)
  link_to_product = models.CharField(max_length=200, null=False)
  type = models.CharField(max_length=30)
  gender = models.CharField(max_length=20)

class Image(models.Model):
  product = models.ForeignKey(product, related_name="images", on_delete = models.CASCADE)
  sift = models.TextField()
  histograms = models.TextField()
